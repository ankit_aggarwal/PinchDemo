package com.example.ankit.pinchdemo;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class PhotosActivity extends AppCompatActivity {

    private RecyclerView mRvPhotos;
    private ProgressDialog mProgressDialog;

    private List<String> mPhotoUris;
    private PhotosAdapter mPhotosAdapter;

    private GridLayoutManager mGridLayoutManager1, mGridLayoutManager2, mGridLayoutManager3;
    private RecyclerView.LayoutManager mCurrentLayoutManager;

    private ScaleGestureDetector mScaleGestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);

        //setup progress dialog
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Fetching Photos...");
        mProgressDialog.setCancelable(false);

        //setup recycler view
        mRvPhotos = (RecyclerView) findViewById(R.id.rv_photos);

        if (mRvPhotos != null) {

            //initialize layout managers
            mGridLayoutManager1 = new GridLayoutManager(this, 1);
            mGridLayoutManager2 = new GridLayoutManager(this, 2);
            mGridLayoutManager3 = new GridLayoutManager(this, 3);

            //initialize photo uris list
            mPhotoUris = new ArrayList<>();

            //initialize adapter
            mPhotosAdapter = new PhotosAdapter(mPhotoUris);

            //set layout manager
            mCurrentLayoutManager = mGridLayoutManager2;
            mRvPhotos.setLayoutManager(mGridLayoutManager2);

            //set adapter
            mRvPhotos.setAdapter(mPhotosAdapter);

            //set scale gesture detector
            mScaleGestureDetector = new ScaleGestureDetector(this, new ScaleGestureDetector.SimpleOnScaleGestureListener() {
                @Override
                public boolean onScale(ScaleGestureDetector detector) {

                    //wrong logic
                    /*if (detector.getScaleFactor() < 0.8) {
                        if (mCurrentLayoutManager != mGridLayoutManager3) {
                            mCurrentLayoutManager = mGridLayoutManager3;
                            mRvPhotos.setLayoutManager(mGridLayoutManager3);
                        }
                    } else if (detector.getScaleFactor() < 1.2) {
                        if (mCurrentLayoutManager != mGridLayoutManager2) {
                            mCurrentLayoutManager = mGridLayoutManager2;
                            mRvPhotos.setLayoutManager(mGridLayoutManager2);
                        }
                    } else {
                        if (mCurrentLayoutManager != mGridLayoutManager1) {
                            mCurrentLayoutManager = mGridLayoutManager1;
                            mRvPhotos.setLayoutManager(mGridLayoutManager1);
                        }
                    }*/

                    //wrong logic
                    /*if (detector.getScaleFactor() < 0.2) {
                        if (mCurrentLayoutManager == mGridLayoutManager1) {
                            mCurrentLayoutManager = mGridLayoutManager2;
                            mRvPhotos.setLayoutManager(mGridLayoutManager2);
                        } else if (mCurrentLayoutManager == mGridLayoutManager2) {
                            mCurrentLayoutManager = mGridLayoutManager3;
                            mRvPhotos.setLayoutManager(mGridLayoutManager3);
                        }
                    } else if (detector.getScaleFactor() > 1.2) {
                        if (mCurrentLayoutManager == mGridLayoutManager3) {
                            mCurrentLayoutManager = mGridLayoutManager2;
                            mRvPhotos.setLayoutManager(mGridLayoutManager2);
                        } else if (mCurrentLayoutManager == mGridLayoutManager2) {
                            mCurrentLayoutManager = mGridLayoutManager1;
                            mRvPhotos.setLayoutManager(mGridLayoutManager1);
                        }
                    }*/

                    //correct logic
                    if (detector.getCurrentSpan() > 200 && detector.getTimeDelta() > 200) {
                        if (detector.getCurrentSpan() - detector.getPreviousSpan() < -1) {
                            if (mCurrentLayoutManager == mGridLayoutManager1) {
                                mCurrentLayoutManager = mGridLayoutManager2;
                                mRvPhotos.setLayoutManager(mGridLayoutManager2);
                                return true;
                            } else if (mCurrentLayoutManager == mGridLayoutManager2) {
                                mCurrentLayoutManager = mGridLayoutManager3;
                                mRvPhotos.setLayoutManager(mGridLayoutManager3);
                                return true;
                            }
                        } else if(detector.getCurrentSpan() - detector.getPreviousSpan() > 1) {
                            if (mCurrentLayoutManager == mGridLayoutManager3) {
                                mCurrentLayoutManager = mGridLayoutManager2;
                                mRvPhotos.setLayoutManager(mGridLayoutManager2);
                                return true;
                            } else if (mCurrentLayoutManager == mGridLayoutManager2) {
                                mCurrentLayoutManager = mGridLayoutManager1;
                                mRvPhotos.setLayoutManager(mGridLayoutManager1);
                                return true;
                            }
                        }
                    }

                    return false;
                }

                @Override
                public void onScaleEnd(ScaleGestureDetector detector) {
                    super.onScaleEnd(detector);

                    /*if (detector.getCurrentSpan() > 200) {
                        if (detector.getCurrentSpan() - detector.getPreviousSpan() < 0) {
                            if (mCurrentLayoutManager == mGridLayoutManager1) {
                                mCurrentLayoutManager = mGridLayoutManager2;
                                mRvPhotos.setLayoutManager(mGridLayoutManager2);
                            } else if (mCurrentLayoutManager == mGridLayoutManager2) {
                                mCurrentLayoutManager = mGridLayoutManager3;
                                mRvPhotos.setLayoutManager(mGridLayoutManager3);
                            }
                        } else if(detector.getCurrentSpan() - detector.getPreviousSpan() > 0) {
                            if (mCurrentLayoutManager == mGridLayoutManager3) {
                                mCurrentLayoutManager = mGridLayoutManager2;
                                mRvPhotos.setLayoutManager(mGridLayoutManager2);
                            } else if (mCurrentLayoutManager == mGridLayoutManager2) {
                                mCurrentLayoutManager = mGridLayoutManager1;
                                mRvPhotos.setLayoutManager(mGridLayoutManager1);
                            }
                        }
                    }*/
                }
            });

            //set touch listener on recycler view
            mRvPhotos.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    mScaleGestureDetector.onTouchEvent(event);
                    return false;
                }
            });

            //fetch photos from gallery
            new FetchPhotosTask(this).execute();
        }
    }

    public static class FetchPhotosTask extends AsyncTask<Void, Void, List<String>> {

        private WeakReference<Context> mContextWeakReference;

        public FetchPhotosTask(Context context) {
            mContextWeakReference = new WeakReference<>(context);
        }

        @Override
        protected void onPreExecute() {
            Context context = mContextWeakReference.get();
            if (context != null) {
                ((PhotosActivity) context).mProgressDialog.show();
            }
        }

        @Override
        protected List<String> doInBackground(Void... params) {

            Context context = mContextWeakReference.get();

            if (context != null) {
                //get photos from gallery
                String[] projection = new String[]{
                        MediaStore.Images.Media.DATA,
                };

                Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

                Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);

                if (cursor != null) {
                    List<String> photoUris = new ArrayList<>(cursor.getCount());
                    while (cursor.moveToNext()) {
                        photoUris.add("file://" + cursor.getString(0));
                    }
                    cursor.close();

                    return photoUris;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<String> photoUris) {
            Context context = mContextWeakReference.get();
            if (context != null) {
                ((PhotosActivity) context).mProgressDialog.dismiss();

                if (photoUris != null && photoUris.size() > 0) {
                    ((PhotosActivity) context).mPhotoUris.clear();
                    ((PhotosActivity) context).mPhotoUris.addAll(photoUris);
                    ((PhotosActivity) context).mPhotosAdapter.notifyDataSetChanged();
                }
            }
        }
    }
}
